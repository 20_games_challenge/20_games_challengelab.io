const formatString = (str) => {
  return str.replace(/\(|\)/g, "").toUpperCase();
};

export const STATE = {
  UNSORTED: "UNSORTED",
  ASCENDING: "ASCENDING",
  DESCENDING: "DESCENDING",
};

export const ROTATED_CLASS = "rotated";
export const HIDDEN_CLASS = "hidden";

const sortAscending = (a, b) => {
  if (a < b) {
    return -1;
  } else if (a > b) {
    return 1;
  } else {
    return 0;
  }
};

const sortDescending = (a, b) => {
  if (a < b) {
    return 1;
  } else if (a > b) {
    return -1;
  } else {
    return 0;
  }
};

export const sortTables = (sideEffect = () => {}) => {
  const tables = [...document.querySelectorAll("table")];

  if (tables.length === 0) {
    return;
  }

  const headerState = {};

  tables.forEach((table) => {
    const headers = table.querySelectorAll("thead th");
    const body = table.querySelector("tbody");
    headers.forEach((header, index) => {
      headerState[index] = STATE.UNSORTED;
      header.addEventListener("click", () => {
        const rows = [...body.querySelectorAll("tr")];
        const sortedRows = rows.sort((rowA, rowB) => {
          const relevantElementA = formatString(
            rowA.children[index].textContent
          );
          const relevantElementB = formatString(
            rowB.children[index].textContent
          );

          if (
            headerState[index] === STATE.UNSORTED ||
            headerState[index] === STATE.DESCENDING
          ) {
            return sortAscending(relevantElementA, relevantElementB);
          } else {
            return sortDescending(relevantElementA, relevantElementB);
          }
        });

        if (
          headerState[index] === STATE.UNSORTED ||
          headerState[index] === STATE.DESCENDING
        ) {
          headerState[index] = STATE.ASCENDING;
        } else {
          headerState[index] = STATE.DESCENDING;
        }

        header.dataset.state = headerState[index];

        body.innerHTML = "";

        sortedRows.forEach((row) => {
          body.appendChild(row);
        });

        sideEffect(header, headers);
      });
    });
  });
};
