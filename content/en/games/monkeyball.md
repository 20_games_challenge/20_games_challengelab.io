---
title: "Super Monkey Ball"
anchor: "monkeyball"
weight: 80
---

Super Monkey Ball is a physics-based platformer from 2001. Monkey Ball started as an arcade unit, but the game was brought to the GameCube (and had the "Super" added to the name). The game was a unique combination of first-person play and physics-based control.

Instead of controlling the character directly, the player would change the slope of the level, causing the ball to roll around. Super Monkey Ball came with multiple race modes and mini-games. Some game modes allowed the player to control the ball directly instead of tilting the level.

| ***Difficulty*** |                                                                                      |
| :---             | :---                                                                                 |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}}                          | 

### Goal:
* Create a handful of game levels, each with a starting area and an ending area. You can make the level flat, or add some 3D slopes and hills.
* Create a sphere representing the player. (You can add a character inside the "ball," or stick with a simple sphere)
* There are two possible control styles. You can either apply forces to the ball directly, causing it to roll, or change the slope of the map, allowing gravity to roll the ball. You can focus on one control scheme if you prefer.
* Create a banana collectable, and place multiple collectables in each level.
* Add a menu and a UI with a score counter, life counter, and timer.
* Add checks for the different win and fail conditions. The player loses a life if they run out of time or fall off of the level. The player wins when they touch the goal.

### Stretch goal:
* Make a full set of 10 or more levels. Sort the levels by difficulty, creating a sense of progression. Keep the player's score and life counter from level to level.

{{< expand "Showcase" >}} {{< include file="showcase/monkeyball.md" type=page >}} {{< /expand >}}
