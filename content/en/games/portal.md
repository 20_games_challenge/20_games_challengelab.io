---
title: "Portal"
anchor: "portal"
weight: 101
---

Portal was released by Valve as a part of the Orange Box bundle in 2007. Portal's mind-bending teleportation puzzles and witty humor made it an instant classic.

Portal's most iconic game mechanic is of course the portals themselves. This game is a good example of computers not understanding "intuitive" mechanics. Portals make sense conceptually, but the game designers had to solve a lot of problems in order to get them to work. In your version, you'll have to manually teach to computer to understand basic physics and light mechanics.

| ***Difficulty*** |                                                                                 |
| :---             | :---                                                                            |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}}                     | 

### Goal:
* Create a 3D first-person controller. The player should be able to walk, run, and jump. The player should have limited "air control" when falling, and they should be able to move at high speeds (though they should have a reasonable terminal velocity)
* Create one or more "test chambers." You can re-create a level from the game, or make your own.
  * Add buttons, boxes, and moving platforms. You can make more puzzle elements if you want.
* Create a portal gun. The gun will place a portal where the player shoots. (The portal will probably require a custom shader and custom physics rules).
  * There are two portals, orange and blue. Portals will move objects or the player to the other portal while conserving momentum.
  * The player can see through portals (and see other portals through the portal, possibly recursively).
  * Portals can only be created on portal-able surfaces. However, portals can be placed anywhere on the surface. If a portal is too close to the edge of a surface, or another portal, it should be placed in a reasonable location near the crosshairs.

### Stretch goal:
* Refine the portal mechanic. Add custom shaders or particle effects to the portal to make the science-fiction magic seem more believable. Ensure that every corner case works like you would expect.
* Add more mechanics and/or levels.
* Don't forget to make a cake. Don't give it to the player. The cake is a lie.

{{< expand "Showcase" >}} {{< include file="showcase/portal.md" type=page >}} {{< /expand >}}
