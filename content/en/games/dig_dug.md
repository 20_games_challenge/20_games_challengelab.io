---
title: "Dig Dug"
anchor: "dig_dug"
weight: 72
---

Dig Dug is a "strategic mining game" released by Namco in 1982. This arcade game featured the very cute premise that you can kill animals by inserting a bicycle pump under their skin, then inflating them until they pop like a balloon!

| ***Difficulty*** |                                                                  |
| :---             | :---                                                             |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}}                          | 

### Goal:
* Create a tile-based level. The world is made of dirt tiles (with different colors for different layers).
* Create the protagonist. You can walk and dig up, down, left, or right. Digging into a tile will destroy it, turning it into an air tile.
* Create enemies. They will spawn in open spaces (3-5 connected tiles) and will walk back and forth in their spaces.
  * Every now and then, an enemy will "burrow" towards the player, turning into a pair of eyes as they move through the dirt. Enemies will stop burrowing when they reach an air tile.
* Create rocks. When the tile below a rock is dug out, the rock will shake for a second, then will fall. It will kill anyone that it collides with (including the player).
* Give the player an "air pump" weapon. They will shoot a hose in the direction that they are facing. If it connects with an enemy, the enemy will swell up and explode.
* Add a score counter, a life counter, and a level counter. Enemies will get faster with each level.

### Stretch goal:
* Create a starting animation for the game. Dig Dug will start on the surface and burrow down into the center of the level before the game starts.

{{< expand "Showcase" >}} {{< include file="showcase/dig_dug.md" type=page >}} {{< /expand >}}
