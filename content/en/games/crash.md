---
title: "Crash Bandicoot"
anchor: "crash"
weight: 82
---

In 1996, Crash Bandicoot released for the PlayStation. The developers wanted to make a 3D character platformer game, but didn't want the player to only see the backside of the character. During the game's design, the team also realized that a full 3D world would be quite boring when compared to a linear 2D world like Sonic's. The solution was to remove a dimension or two from each level. Some levels were a side-scrolling platformer, some were liner corridors, and others were time-constrained. The end result was a much faster-paced game than a more open platformer like Super Mario 64.

For this challenge, just focus on the 2.5D platforming segments. If you want, you can expand to other level types from there.

| ***Difficulty*** |                                                                                      |
| :---             | :---                                                                                 |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} | 

### Goal:
* Create a 2D platformer level with 3D assets. The world should contain platforms, spikes, crates, and moving enemies.
  * There are many possible hazards, but you should at least make something that has to be avoided, something that can be defeated by jumping on it, and something that can be defeated with a spin-attack.
* Create a character. The character should be able to walk, jump, and spin-attack. They will be primarily moving in 2 dimensions, but you can add the ability to move in full 3D space if you want. (This would be necessary if you want to make more level types.)
* Add a score counter and life counter. The player can increase their score by breaking crates in the level. 


### Stretch goal:
* Create one or more additional level type:
  * Over-the-shoulder - Crash is running into the screen down a narrow corridor.
  * Boulder Chase - Crash is running into the screen. He is being chased by a boulder (and must therefore run at full speed)
  * Hog wild - Crash is riding a wild hog at full speed (same as the boulder chase, but moving in the opposite direction)
  * 3D level - Combine over-the-shoulder sections with 2.5D platforming sections in the same level.

{{< expand "Showcase" >}} {{< include file="showcase/crash.md" type=page >}} {{< /expand >}}
