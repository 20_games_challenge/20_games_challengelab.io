---
title: "Motherload"
anchor: "motherload"
weight: 73
---

Here are some details about this game!

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}}                          | 

### Goal:
* 

### Stretch goal:
* 

{{< expand "Showcase" >}} {{< include file="showcase/motherload.md" type=page >}} {{< /expand >}}
