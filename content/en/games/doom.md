---
title: "Doom"
anchor: "doom"
weight: 90
---

Doom is widely known as the father of the FPS genre. Id software released Wolfenstein 3D in 1992. Doom was released at the end of 1993, just a year and a half later. The game engine was a refinement of Wolfenstein's ideas, with more advanced mechanics and a more complex 3D engine. The game uses 2D sprites in a 3D world (sometimes called "2.5D"), but the player was able to move in any direction, and the world contained a lot of elevation and varied geometry.

Doom had many imitators, but the "doom clones" eventually began to diverge, and they were eventually renamed to "FPS games." Some of Doom's ideas seem outdated now, but the game still influences many of its successors even today.

| ***Difficulty*** |                                                                                      |
| :---             | :---                                                                                 |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} | 

### Goal:
* Create a first person controller for the player. The player should be able to look up, down, left, and right. They should also be able to walk forwards and backwards, as well as strafe and jump.
* Create one or more "arena" levels. Arenas can be built from primitive shapes, but they should have some verticality (slopes, stairs, or elevators).
* Add one or more enemy variants. Enemies should wait until the player is nearby, then shoot or attack the player. Feel free to play with more advanced movement/timing mechanics.
* Add two or more weapons. You should make 1. a hitscan weapon that deals instant damage, and 2. a projectile weapon that fires a physical bullet which will travel to the target.
* Add a UI with a health bar and ammo count. The player will receive damage when attacked, and will die when their health reaches zero.
* Add a health pickup that restores player health and an ammo pickup that increases the player's ammo count.

### Stretch goal:
* In Doom 1993, enemies were billboard sprites (2D images in 3D space), but you can make fully 3D enemies if you want.
* Feel free to play with AI behaviors for the enemies. You can make an "Ai director" that manages when each enemy can move or attack.

{{< expand "Showcase" >}} {{< include file="showcase/doom.md" type=page >}} {{< /expand >}}
