---
title: "Welcome to the 20 games challenge!"
anchor: "introduction"
weight: 10
---

{{< youtube Ix0YxEZ-c4U >}}
A quick intro video if you're not into reading :)

## The 20 Games Challenge is a fun way to gain confidence while learning to make video games.
If you take a traditional art class, you might be asked to re-create a famous work. It might sound like a waste of time at first, but by copying a masterpiece, you will often gain a deeper understanding of the fundamentals than you could gain by just "winging it."

Video games are one of the most complex art forms to date. Mastering the fundamentals of game development will take a lot of practice and patience. Many professional game developers will recommend making multiple small projects instead of starting with your "dream game."

This challenge is my attempt to simplify the learning process for anyone who wants to start making their own video games.

## The 20 Games Challenge is a codified approach to deliberately learning the fundamentals of game development.

The goal of the 20 Games Challenge is to ***learn game development in 20 games or less.*** You don't have to complete all 20 games, but I would recommend finishing at least 10. You should also set some final goal for yourself; like creating an RTS or a full FPS. You know why you want to make games, so let the destination guide your unique journey.

Every step is gradual, so you won't be learning too many things at once. You can, of course, make things as easy or difficult for yourself as you want.

{{< hint type=tip icon=question-circle title=". Is this just a fancy game jam?" >}}
The 20 Games Challenge is not a jam. [Game jams](https://itch.io/jams) are a great way to gain experience, but they're not for everyone. Most jams come with strict time limits, which are limiting and can sometimes lead to bad habits like crunch and burnout cycles.

Jams might be your jam. Maybe you want to do the 20 Games Challenge, but substitute in a jam or two. Whatever works for you, works! The ultimate goal is to become a confident and competent game developer.
{{< /hint >}}

I compiled a list of some of the most popular and successful games ever made. This isn't a comprehensive list. Rather, I've selected games that build on the fundamentals and slowly introduce concepts over time. Once you get the hang of it, you can start substituting more games in and going off-script. There are more than 20 games in total, so you'll want to chose the 20 games that fit you best.

## Start your journey:

[Here]({{< ref "how/_index.md" >}}) are some more rules and details for the challenge.

You can also check out the [curated list of games]({{< ref "challenge/_index.md" >}}) (or just pick a game from the [master list.]({{< ref "games/_index.md" >}}))

{{< hint title="Looking for support?">}}
There is now a community managed [Discord Server](https://discord.gg/mBGd9hahZv) dedicated to the challenge.

You can also use the hashtag [#20_games_challenge](https://twitter.com/hashtag/20_games_challenge) to share your work and see what others have done!
{{< /hint >}}

### One final note:

I want to make this resource as helpful as possible. The [website is open-sourced](https://gitlab.com/20_games_challenge/20_games_challenge.gitlab.io), so any feedback, suggestions, or contributions are appreciated!  
Please check the [contributions]({{< ref "contributing/_index.md" >}}) page for more details.