---
title: "Lunar Lander Showcase"
anchor: "lander_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* theMinesAreShakin made a [web playable game](https://theminesareshakin.itch.io/lander) with a [devlog](https://jakemeinershagen.com/lander)

{{< include file="_parts/showcase_footer.md" type=page >}}
