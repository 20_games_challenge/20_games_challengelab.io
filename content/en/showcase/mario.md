---
title: "Super Mario Bros Showcase"
anchor: "mario_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* steflo made a [web playable game](https://steflo.itch.io/zig-mario) in Zig with [source code](https://github.com/stefanpartheym/zig-mario)

* ForsakenVoid made a [web playable game](https://forsakenvoid.itch.io/mario-bros-remake)

* Erip made a [web playable game](https://games.petzel.io/mario/)

* crazillo made a [web playable game](https://homefrog-games.itch.io/super-mario-bros)

* BoofGall Games made a [web playable game](https://boofgall-games.itch.io/amus-the-mushroom) with a trailer:
  {{< youtube 9C4LkHLrZmc >}}

* Eka made a [web playable game](https://yakdoggames.itch.io/the-barroom-uprisers) with gameplay footage:
  {{< youtube 6psdATqrn-g >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/jump-man):
  {{< youtube uzu6PchSxoY >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
