---
title: "Pac_Man Showcase"
anchor: "pacman_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Cevantime made a [web playable game](https://cevantime.itch.io/pacman-challenge) with [source code](https://github.com/Cevantime/pacman-challenge)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/zep-man) with [source code](https://github.com/AlixBarreaux/zep-man)

* Dale made a [web playable game](https://dalekent.itch.io/dales-20-game-challenge) with [source code](https://github.com/DaleMods/20_game_challenge)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/pacman-20-games-challenge)

* Jdog made a [web playable game](https://doodledonut.itch.io/pacman)

* ochunks made a [web playable game](https://o-chunks.itch.io/harvest-hop)

* bobitu_ made a [web playable game](https://bobitu.itch.io/pacman)

* Erip made a [web playable game](https://games.petzel.io/pacman/)

* Greg made a [web playable game](https://an-unique-name.itch.io/pac-clone)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/pac-man-5-in-the-20-games-challenge)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/realm-of-pac)

* BoofGall Games made a [web playable game](https://boofgall-games.itch.io/pacman)

* Eka made a [downloadable game](https://yakdoggames.itch.io/astro-man) with gameplay footage:
  {{< youtube 3Mb4MmUhG2c >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/pac-man):
  {{< youtube 221sPOp_514 >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
