---
title: "Tic Tac Toe Showcase"
anchor: "tic_tac_toe_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* bobitu_ made a [web playable game](https://bobitu.itch.io/tictactoe)

* Triss.exe made a [web playable game](https://trissexe.itch.io/tic-tac-toe)

* mayzavan made a game with [source code](https://github.com/mayzavan/7.Tic-Tac-toe)

{{< include file="_parts/showcase_footer.md" type=page >}}
