---
title: "Frogger Showcase"
anchor: "frogger_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* theMinesAreShakin made a [web playable game](https://theminesareshakin.itch.io/toader) with [source code](https://jakemeinershagen.com/toader)

* Leander made a [web playable game](https://leandres.itch.io/frog-game)

* Kensei made a [web playable game](https://kensei95.itch.io/frogs-life)

* BaconEggs made a [web playable game](https://baconeggsrl.itch.io/froggy-hop)

* Cevantime made a [web playable game](https://cevantime.itch.io/frogger-challenge)

* TheVedect made a [web playable game](https://thevedect.itch.io/frogy)

* bobitu_ made a [web playable game](https://bobitu.itch.io/frogger)

* Oyis made a [web playable game](https://flappyturd.itch.io/the-frog-tastic-adventure)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/frogger-20-games-challenge)

* Erip made a [web playable game](https://games.petzel.io/frogger/index.html)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/frogger-clone)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/the-20-games-challenge)

* Xyloph made a [web playable game](https://xyloph.itch.io/paper-frogger)

* mayzavan made a [web playable game](https://mayzavan.itch.io/20gc-frogger)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/frogger):
  {{< youtube XO3JjkDpd34 >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
