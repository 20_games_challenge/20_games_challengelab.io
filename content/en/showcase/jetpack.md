---
title: "Jetpack Joyride Showcase"
anchor: "jetpack_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Brallex made a [web playable game](https://brallex.itch.io/jetpack-joyride) with [source code](https://github.com/Alexander-Jordan/jetpack-joyride-godot)

* Andrew made a [web playable game](https://tourmaline-trifle-efc59f.netlify.app) with [source code](https://gitlab.com/frenata-20-game-challenge/jetpack)

* Carson made a [web playable game](https://cmckinstry.itch.io/cadet-dash) with [source code](https://github.com/CarsonMcKinstry/cadet-dash-20-game-challenge)

* StarHawk made a [web playable game](https://thestarhawk.itch.io/slime-escape) with [source code](https://github.com/TheStarHawk/SlimeEscape/releases/tag/v1.0)

* nadi726 made a [web playable game](https://kitao.github.io/pyxel/wasm/launcher/?play=nadi726.Rocket-Flight.dist.rocket-flight) with [source code](https://github.com/nadi726/Rocket-Flight)

* Gorman made a [downloadable game](https://gorman-play.itch.io/mesa-project) with [source code](https://github.com/GormanProg123/Mesa-Jetpack)

* mattos_sus made a [downloadable game](https://marcs-sus.itch.io/jetpack-nah-wand) with [source code](https://github.com/marcs-sus/jetpackJoyride-game02)

* Goldenbeard made a [web playable game](https://goldenbeard.itch.io/brap-bastard)

* RogueAbyss made a [web playable game](https://rogueabyss.itch.io/bubble-mage)

* avratz made a [web playable game](https://avratz.itch.io/runic-rush)

* tumati made a [web playable game](https://tumati.itch.io/runaway-saucer)

* Cevantime made a [web playable game](https://cevantime.itch.io/jetpack-remake)

* Vera made a [web playable game](https://verounsen.itch.io/jetpack-marshmallow)

* Kensei made a [web playable game](https://kensei95.itch.io/rocket-dino)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/jetpack-joyride)

* Oyis made a [web playable game](https://flappyturd.itch.io/the-joy-of-jetpacks)

* Jimothy made a [web playable game](https://gx.games/games/8vccin/hoverboard-joyride/)

* Juan Carlos made a [web playable game](https://1juancarlos.itch.io/jumbas-joyride)

* CakeBlood made a [web playable game](https://cakeblood.itch.io/sleigh-rider)

* ochunks made a [web playable game](https://oelias.itch.io/broomstick-joyride)

* Triss.exe made a [web playable game](https://trissexe.itch.io/20-games-challenge-jet-pack-clone)

* Xavire94 made a [web playable game](https://xavire94.itch.io/spooky-joyride)

* TheVedect made a [web playable game](https://thevedect.itch.io/capsule-joyride)

* TheFirenze made a [web playable game](https://klausmerrit.itch.io/jetpack-alien)

* mayzavan made a [web playable game](https://mayzavan.itch.io/20gc-jetpackjoyride)

* Eka made a [downloadable game](https://yakdoggames.itch.io/nick-deck-jet)

* Nate made a [downloadable game](https://buzjr.itch.io/jetpack-joyride) and a video devlog:
  {{< youtube bIySGIJCALk >}}

* Luke Muscat (the original designer for Jetpack Joyride) made an interesting video about the game:<br>(The video wasn't for this challenge, I just thought it was an interesting resource to link)
  {{< youtube mxHkXADm3gU >}} 

{{< include file="_parts/showcase_footer.md" type=page >}}
