---
title: "Breakout Showcase"
anchor: "breakout_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Avram made a [web playable game](https://avrame.itch.io/breakout-clone) with [source code](https://github.com/avrame/breakout_clone)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/simple-breakout) with [source code](https://github.com/AlixBarreaux/simple-breakout)

* Dale made a [web playable game](https://dalekent.itch.io/dales-20-game-challenge) with [source code](https://github.com/DaleMods/20_game_challenge)

* Brallex made a [web playable game](https://brallex.itch.io/breakout) with [source code](https://github.com/Alexander-Jordan/breakout-godot)

* NoFoxTuGiv made a [downloadable game](https://github.com/NoFoxTuGiv/YABOC/releases/tag/1.0.0.0) with [source code](https://github.com/NoFoxTuGiv/YABOC)

* cantaim made a [web playable game](https://func-menigoi.itch.io/break-out) with [source code](https://github.com/nigoi/breakout_clone)

* Revlos made a [web playable game](https://revlos.itch.io/breakout-clone)

* CozmoSlug6X made a [web playable game](https://cozmo-6.itch.io/breakout-clone)

* Old80sBits made a [web playable game](https://astralbyte.co.nz/20Games/2_Breakout/)

* Xithas made a [web playable game](https://xithas.itch.io/scribble-breakout)

* Speiro made a [web playable game](https://speiro.itch.io/block-break)

* mjkjr made a [web playable game](https://mjkjr.itch.io/rectangle-pop-breakout-clone)

* Kensei made a [web playable game](https://kensei95.itch.io/breakout)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/breakout)

* DNSik made a [web playable game](https://he-kireki.itch.io/breakoutchallenge)

* pseudoLudo made a [web playable game](https://pseudoludophile.itch.io/breakout)

* MadeFromScrap made a [web playable game](https://cwheeler.itch.io/20-games-challenge-breakout)

* Mstach made a [web playable game](https://willianmstach.itch.io/breakoudd)

* ceafin made a [web playable game](https://ceafin.itch.io/20-games-challenge-2-breakanoid)

* Leander made a [web playable game](https://leandres.itch.io/break-bricks)

* SereneJellyfish made a [web playable game](https://serenejellyfishgames.itch.io/beachside-breakout)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/generic-breakout-clone)

* Martal made a [web playable game](https://martialis39.itch.io/brick-game)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/breakwave)

* Jackson made a [web playable game](https://districtjackson.itch.io/break-the-breakout)

* Erip made a [web playable game](https://games.petzel.io/breakout/index.html)

* Woofenator made a [web playable game](https://woofenator.itch.io/break-in)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/breakout-clone)

* Walid made a [web playable game](https://waliddib.itch.io/breakout)

* mayzavan made a [web playable game](https://mayzavan.itch.io/20gc-breakout)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/breakout-20-games-challenge)

* TheVedect made a [web playable game](https://thevedect.itch.io/juicy-breakout)

* yaastra made a [web playable game](https://aklesh888.itch.io/break-out)

* storm strife made a [web playable game](https://neospare.itch.io/breakout-mini)

* Scholar_NZ made a [web playable game](https://scholar-nz.itch.io/breakout-by-scholar-nz)

* Cuzgun made a [web playable game](https://cuzgun.itch.io/brickout)

* Saleem121 made a [downloadable game](https://saleem121.itch.io/breakout-clone)

* CheopisIV made a [downloadable game](https://cheopisiv.itch.io/breaker-2023)

* Cat/Jess made a [downloadable game](https://solipsisdev.itch.io/brick-break)

* Zyx750 made a [downloadable game](https://zyx750.itch.io/breakout)

* Xyloph made a game with [source code](https://github.com/Xyloph/20GC_Game2)

* jester038 made a game with [source code available](https://github.com/kylem038/Breakout). The game is downloadable under the Releases tab.

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/breakout):
  {{< youtube xbw1DKH0pqA >}}

* Derik.Digital shared a gameplay video with some advanced effects!
  {{< youtube tHbHAJUVkLE >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
