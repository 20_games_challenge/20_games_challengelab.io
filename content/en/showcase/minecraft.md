---
title: "Minecraft Showcase"
anchor: "minecraft_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Dale made a [web playable game](https://dalekent.itch.io/dales-20-game-challenge) with [source code](https://github.com/DaleMods/DaleCraft)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/minecraft):
  {{< youtube 1nPqzq9ocLA >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
