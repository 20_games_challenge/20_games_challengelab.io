---
title: "Spacewar! Showcase"
anchor: "spacewar_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* mjkjr made a [web playable game](https://mjkjr.itch.io/pizza-battle-in-space-spacewar-clone)

* ochunks made a [web playable game](https://o-chunks.itch.io/space-war)

* bobitu_ made a [web playable game](https://bobitu.itch.io/spacewar)

* Crowilly made a [web playable game](https://crowilly.itch.io/spacewar)

* berni made a [downloadable game](https://suddenmoustachegames.itch.io/spacewar)

{{< include file="_parts/showcase_footer.md" type=page >}}
