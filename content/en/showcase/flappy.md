---
title: "Flappy Bird Showcase"
anchor: "flappy_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Cevantime made a [web playable game](https://cevantime.itch.io/flappy-bird-challenge)

* Avram made a [web playable game](https://avrame.itch.io/flappy-bird-clone) with [source code](https://github.com/avrame/flappy-bird-clone)

* Niashi made a [web playable game](https://niashi24.github.io/Projects/FlappyBird/index.html) with [source code](https://github.com/Niashi24/FlappyClone)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/flappy-hat) with [source code](https://github.com/AlixBarreaux/flappy-hat)

* Ceafin made a [web playable game](https://ceafin.itch.io/20-games-challenge-1-flappy-bird) with [source code](https://github.com/ceafin/20GC_Challenge01_FlappyBird/)

* Mstach made a [web playable game](https://willianmstach.itch.io/flappybat) with a [devlog](https://willianmstach.wixstudio.io/mstachdev/post/flappy-bat-tap-tap-tap)

* MNSHKV made a [web playable game](https://mnshkv.itch.io/flappyplane) with [source code](https://github.com/mnshkv3d/01_FlappyPlane)

* cantaim made a [web playable game](https://func-menigoi.itch.io/fly-icarus) with [source code](https://github.com/nigoi/flappy_clone)

* RogueAbyss made a [web playable game](https://rogueabyss.itch.io/flappy-cherry)

* lost made a [web playable game](https://abstractlayer.itch.io/chickyfly)

* Revlos made a [web playable game](https://revlos.itch.io/flappy-clone)

* 8Mobius8 made a [web playable game](https://8mobius8.itch.io/flappy-bones)

* mjkjr made a [web playable game](https://mjkjr.itch.io/buzzy-bee-flappy-bird-clone)

* Kensei made a [web playable game](https://kensei95.itch.io/flappy-bird)

* BaconEggs made a [web playable game](https://baconeggsrl.itch.io/flappy-birb)

* Verounsen made a [web playable game](https://verounsen.itch.io/flappy-cube)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/floppy-bird)

* Grumpy made a [web playable game](https://92ninetwo.itch.io/falbbybirb)

* barni made a [web playable game](https://barneyo.itch.io/1-bit-droopy-eye)

* myles made a [web playable game](https://mylesgames.itch.io/pac-bird)

* Valumin made a [web playable game](https://valumin.itch.io/flappy-ufo)

* Xavire94 made a [web playable game](https://xavire94.itch.io/flappy-space-adventure)

* txorimalo made a [web playable game](https://txorimalo.itch.io/helloucar)

* CakeBlood made a [web playable game](https://cakeblood.itch.io/flappy-gunnar)

* Bumpi made a [web playable game](https://bumpi.itch.io/another-flappy-bird-clone)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/flappy-thingy)

* Jimothy made a [web playable game](https://gx.games/games/dyizk2/flappy-eagle/)

* Nazorus made a [web playable game](https://nazorus.itch.io/jetcave)

* AllHeart made a [web playable game](https://gx.games/games/busvgk/flying-fish/tracks/c38f865d-9823-4744-8f00-95ff8f8737a6)

* Erip  made a [web playable game](https://games.petzel.io/flappybird/index.html)

* Juan Carlos made a [web playable game](https://1juancarlos.itch.io/flappy-bird-vanilla)

* Karmanya  made a [web playable game](https://karmanya007.itch.io/elevate-a-winged-journey)

* BanMedo  made a [web playable game](https://banmedo.itch.io/fly-er)

* Boofgall made a [web playable game](https://boofgall-games.itch.io/ballon-survival)

* ochunks made a [web playable game](https://oelias.itch.io/flappy-bird-clone)

* Linkio made a [web playable game](https://linkio.itch.io/stalacflight)

* Silhouette made a [web playable game](https://thesilhouette.itch.io/flappy-bee)

* kametman made a [web playable game](https://kametman.itch.io/duely-planes)

* SanderZw made a [web playable game](https://sanderzw.itch.io/miniflappybird)

* yaastra made a [web playable game](https://aklesh888.itch.io/flappy-bird)

* Maddawik made a [web playable game](https://maddawik.itch.io/flap-kenney) with [source code](https://github.com/maddawik/flappy-thing)

* mayzavan made a [web playable game](https://mayzavan.itch.io/20gc-flappybird)

* ats made a game with [source code](https://github.com/asikora/ctappy-raylib)

* MrKiwi made a game with [source code](https://github.com/sosafacun/Bird-That-Flaps)

* Rooster Syndicate made a [downloadable game](https://rooster-syndicate.itch.io/flappy-cow-a-completely-normal-flappy-bird-clone)

* Sutasu made a [downloadable game](https://sutasu.itch.io/flappy-bat)

* Arnz made a [downloadable game](https://arnz.itch.io/circly-boi)

* Tom is Green made a [downloadable game](https://tomisgreen.itch.io/cyberflap-2084)

* Brallex made a [web playable game](https://brallex.itch.io/flappy-bird) with a video devlog and [source code](https://github.com/Alexander-Jordan/flappy-bird-godot):
  {{< youtube I2wzvSR1DRc >}}

* Gorman made a [downloadable game](https://gorman-play.itch.io/flarpy-bird) with [source code](https://github.com/GormanProg123/Flarpy-Bird)

* Fictional made a [downloadable game](https://fictionalcodes.itch.io/surging-star) with [source code](https://github.com/FictionalAroma/20Games)

* Brallex made a [web playable game](https://brallex.itch.io/flappy-bird) with a video devlog
{{< youtube I2wzvSR1DRc >}}

* trueauracoral made a [web playable game](https://trueauracoral.itch.io/flappy-bird) with a video devlog:
  {{< youtube 62iJqvonbHY >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
