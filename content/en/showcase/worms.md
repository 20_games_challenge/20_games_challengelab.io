---
title: "Worms"
anchor: "worms_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Dale made a [web playable game](https://dalekent.itch.io/dales-20-game-challenge) with [source code](https://github.com/DaleMods/20_game_challenge)
(This one is technically Tanks, but close enough 🙂)

* Greg made a [web playable game](https://an-unique-name.itch.io/almost-worms)

{{< include file="_parts/showcase_footer.md" type=page >}}
