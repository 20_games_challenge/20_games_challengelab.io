---
title: "Space Invaders Showcase"
anchor: "invaders_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Cevantime made a [web playable game](https://cevantime.itch.io/space-invaders-remake)

* Andrew made a [web playable game](https://frenata.itch.io/space-invaders) with [source code](https://gitlab.com/frenata-20-game-challenge/space-invaders)

* ochunks made a [web playable game](https://o-chunks.itch.io/space-invaders) with [source code](https://gitlab.com/orlandoelias/space-invaders)

* MadeFromScreams made a [web playable game](https://cwheeler.itch.io/20-games-challenge-space-invaders) with [source code](https://github.com/ClaytonWheeler0205/20GamesSpaceInvaders)

* StarHawk made a [web playable game](https://thestarhawk.itch.io/invaders-from-the-underdark) with [source code](https://github.com/TheStarHawk/WizardInvader/releases/tag/20GamesChallenge)

* Goldenbeard made a [web playable game](https://goldenbeard.itch.io/sunset-of-the-dead)

* NoFoxTuGiv made a [web playable game](https://nofoxtugiv.com/grots-last-stand)

* Kensei made a [web playable game](https://kensei95.itch.io/space-invaders)

* mjkjr made a [web playable game](https://mjkjr.itch.io/star-defender-space-invaders-clone)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/the-day-the-eldritch-came)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/space-invader)

* Cuzgun made a [web playable game](https://cuzgun.itch.io/polywars)

* Local Man made a [web playable game](https://local-mans.itch.io/invaders)

* T$ made a [web playable game](https://tmichal2.itch.io/space-invaders)

* Xavire94 made a [web playable game](https://xavire94.itch.io/goblin-invaders)

* Saleem121 made a [downloadable game](https://saleem121.itch.io/space-invaders-clone)

* jester038 made a [downloadable game](https://luckyjester.itch.io/space-invaders-godot-csharp)

* TheVedect made a [web playable game](https://thevedect.itch.io/space-invaders) with a video devlog:
  {{< youtube 3-einQMOunc >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/space-invaders):
  {{< youtube kV5N86ULkpw >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
