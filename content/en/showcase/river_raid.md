---
title: "River Raid Showcase"
anchor: "river_raid_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Dale made a [web playable game](https://dalekent.itch.io/dales-20-game-challenge) with [source code](https://github.com/DaleMods/20_game_challenge)

* RogueAbyss made a [web playable game](https://rogueabyss.itch.io/20-game-challenge-3-river-raid)

* Cevantime made a [web playable game](https://cevantime.itch.io/river-raid-challenge)

* Greg made a [web playable game](https://an-unque-name.itch.io/river-raid-learning-project)

* Speiro made a [web playable game](https://speiro.itch.io/river-raid)

* SuddenMoustacheGames made a [web playable game](https://suddenmoustachegames.itch.io/riverraid)

* yaastra made a [web playable game](https://aklesh888.itch.io/river-raid)

* Cat/Jess made a [downloadable game](https://solipsisdev.itch.io/starfighter-strike)

* Eka made a [downloadable game](https://yakdoggames.itch.io/air-driver)

* vanawy made a [web playable game](https://vanawy.itch.io/river-raid)

{{< include file="_parts/showcase_footer.md" type=page >}}
